<?php

    $src=file_get_contents("index.html");

    $endorsements="";
    $endorsement="The dutch Electronic Gulden Foundation endorses communitycoins.org. ";
    $endorsement.="We will put significant effort in the cooperation with other community-coins that support the shared manifest.";
    $img=file_get_contents("http://communitycoins.org/qr.php?ticker=efl&data=".urlencode($endorsement));
    $endorsements.="CC_endorsement[0]='$endorsement|$img'\n";

    $endorsement="As we reflect on the specifics of being a Canadian crypto currency community, ";
    $endorsement.="we recognize that even though we are focused in Canada, ";
    $endorsement.="our work applied to the national level problems also apply to others in their own communities. ";
    $endorsement.="As we work on building tools to help solve user-centric problems, ";
    $endorsement.="we join with other communities who are in a similar state with the intent so share our experiences. ";
    $img=file_get_contents("http://communitycoins.org/qr.php?ticker=cdn&data=".urlencode($endorsement));
    $endorsements.="\tCC_endorsement[2]='$endorsement|$img'\n";

    $endorsement="Auroracoin believes that cryptocurrencies promote individual freedoms and that groups like CommunityCoins.org are essential in building like-minded communities and financial technology infrastructure.";
    $endorsement.="We are committed to supporting CommunityCoins.org through shared knowledge & experience, technical collaboration, goodwill, and positive reinforcement.";
    $img=file_get_contents("http://communitycoins.org/qr.php?ticker=aur&data=".urlencode($endorsement));
    $endorsements.="\tCC_endorsement[6]='$endorsement|$img'\n";

    $endorsement="The Privateness Network is fully committed to it's mission of decisively and definitively provide or return self-sovereignty ";
    $endorsement.="and privacy to the individual and above all preserving his right to choose.";
    $endorsement.="We therefore fully subscribe to the precepts and values promoted in the Communitycoins.org manifest and by it's members who allow us the honor to contribute to this noble endeavour. ";
    $endorsement.="All the resources, contacts, time, knowledge and experiences we can bring to bear to assist will be.";
    $img=file_get_contents("http://communitycoins.org/qr.php?ticker=ness&data=".urlencode($endorsement));
    $endorsement=str_replace("'","\'",$endorsement);
    $endorsements.="\tCC_endorsement[7]='$endorsement|$img'\n";

    $endorsement="Pakcoin aims to bring cryptocurrency usage into daily lives of common people because its by the people for the people. ";
    $endorsement.="We support communitycoins.org by sharing our experiences and technical knowledge for promoting well being of community coins.";
    $img=file_get_contents("http://communitycoins.org/qr.php?ticker=pak&data=".urlencode($endorsement));
    $endorsement=str_replace("'","\'",$endorsement);
    $endorsements.="\tCC_endorsement[8]='$endorsement|$img'\n";

    $endorsement="Sterlingcoin very proudly endorses CommunityCoins.org as an effort to mutually support like-minded and community-driven cryptocurrency projects with knowledge and resource sharing. ";
    $endorsement.="Sterlingcoin is committed to support CommunityCoins.org, its manifest, and peer projects in every way possible.";
    $img=file_get_contents("http://communitycoins.org/qr.php?ticker=slg&data=".urlencode($endorsement));
    $endorsement=str_replace("'","\'",$endorsement);
    $endorsements.="\tCC_endorsement[4]='$endorsement|$img'\n";

    $anchor='<!--ENDORSEMENTS-->';
    $x=strpos($src,$anchor);
    $inject=$endorsements;
    $src=substr($src,0,$x).$inject.substr($src,$x+strlen($anchor));

    echo $src;
?>
